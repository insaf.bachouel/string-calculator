package main.calculator;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringCalculator {

	public static int add(String string) {
		if(!string.isEmpty()) {
			List<Integer> numbers =getNumbersOfString(string);
			checkNegativeNumberPresence(numbers); 
			return numbers.stream().reduce(Integer::sum).get();
		}
		return 0;
	}
	
	private static List<Integer> getNumbersOfString(String string) {
		if(string.startsWith("//")) {
			Matcher matcher =Pattern.compile("//(.)\n(.*)").matcher(string) ;
			if(matcher.matches()) {
				String delimiter = matcher.group(1) ;
				String stringToSplit = matcher.group(2);
				System.out.print(matcher.group(1)); 
				return Stream.of(stringToSplit.split(delimiter)).
						map(Integer::parseInt).collect(Collectors.toList());
			}
		}
		return Stream.of(string.split(",|\n")).
				map(Integer::parseInt).collect(Collectors.toList());
	}
	
	private static void checkNegativeNumberPresence(List<Integer> numbers) {
		 StringBuffer errorMessage = new StringBuffer() ;
		 numbers.stream().forEach((number)-> {
			 if(number<0) {
				 errorMessage.append(number).append(" "); 
			 } });
		 if(!errorMessage.toString().isEmpty()) {
			 errorMessage.insert(0, "negatives not allowed ") ; 
			 throw new NumberFormatException(errorMessage.toString()); 
		 }
		 
	}

	
	

}
